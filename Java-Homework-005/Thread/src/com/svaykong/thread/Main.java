package com.svaykong.thread;

public class Main {
    public static void main(String[] args) {

        WorkerRunnable run1 = new WorkerRunnable("Hello KSHRD!");
        Thread t1 = new Thread(run1);

        WorkerRunnable run2 = new WorkerRunnable("*************************************");
        Thread t2 = new Thread(run2);

        WorkerRunnable run3 = new WorkerRunnable("I will try my best to be here at HRD.");
        Thread t3 = new Thread(run3);

        WorkerRunnable run4 = new WorkerRunnable("-------------------------------------");
        Thread t4 = new Thread(run4);

        WorkerRunnable run5 = new WorkerRunnable("...........");
        Thread t5 = new Thread(run5);

        try {
            t1.start();
            t1.join();
            System.out.println();

            t2.start();
            t2.join();
            System.out.println();

            t3.start();
            t3.join();
            System.out.println();

            t4.start();
            t4.join();
            System.out.println();

            System.out.print("Downloading");
            t5.start();
            t5.join();
            System.out.print("Completed 100%!");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class WorkerRunnable implements Runnable {
    public String message;

    WorkerRunnable(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        for(int i = 0; i < message.length(); i++) {
            System.out.print(message.charAt(i));
            try {
                Thread.sleep(100);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
